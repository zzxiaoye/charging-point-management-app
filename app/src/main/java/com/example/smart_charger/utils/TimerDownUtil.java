package com.example.smart_charger.utils;

import android.os.Handler;
import android.os.Looper;

/**
 * 倒计时
 */
public class TimerDownUtil {

    private final Listener listener;

    private int duration = 0;
    private boolean isCanceled = false;

    private Handler handler = new Handler(Looper.getMainLooper());
    private Runnable mCounterRunnable = new Runnable() {
        @Override
        public void run() {
            if (isCanceled) {
                if (null != listener) {
                    listener.onCanceled();
                }
                return;
            }
            if (duration <= 0) {
                if (null != listener) {
                    listener.onComplete();
                }
            } else {
                handler.postDelayed(this, 1000);
                if (null != listener) {
                    listener.onTick(duration);
                }
            }
            duration--;
        }
    };

    public TimerDownUtil(int durationInSeconds, final Listener listener) {
        this.duration = durationInSeconds;
        this.listener = listener;
    }

    public void start() {
        if (null != listener) {
            listener.onStart();
        }
        handler.post(mCounterRunnable);
    }

    public void cancel() {
        handler.removeCallbacks(mCounterRunnable);
        this.isCanceled = true;
    }

    public interface Listener {

        void onStart();

        void onTick(int seconds);

        void onComplete();

        void onCanceled();
    }
}
