package com.example.smart_charger.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeFormatUtil {

    public static String getTime(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String sd = sdf.format(new Date(time));
        return sd;
    }
}
