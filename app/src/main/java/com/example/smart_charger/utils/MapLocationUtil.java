package com.example.smart_charger.utils;

import android.content.Context;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;

import java.util.HashMap;
import java.util.Map;

public class MapLocationUtil {

    private static int count;

    /**
     * 获取经纬度
     * @param context
     * @param isHighPrecision
     * @param mLocationCallBack
     */
    public static void getLocation(Context context, boolean isHighPrecision, final LocationCallBack mLocationCallBack) {
        AMapLocationClient mAMapLocationClient = new AMapLocationClient(context);
        //设置定位回调监听
        mAMapLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                Map<String, Object> map = new HashMap<>();
                if (null != aMapLocation) {
                    int errorCode = aMapLocation.getErrorCode();
                    map.put("code", errorCode);
                    if(errorCode != 0) {
                        Log.d("location", "错误码 == " + errorCode);
                        mLocationCallBack.callBack(map);
                        return;
                    }
                    String address = aMapLocation.getAddress();
                    String lon = String.valueOf(aMapLocation.getLongitude());
                    String lat = String.valueOf(aMapLocation.getLatitude());
                    Log.d("location", "address == " + address + "   lon == " + lon + "   lat == " + lat);
                    // 把数据放进successCallback中，返回给JS端

                    // 定位信息
                    String locationType = String.valueOf(aMapLocation.getLocationType());
                    String locationDetail = aMapLocation.getLocationDetail();
                    String locationGpsAccuracyStatus = String.valueOf(aMapLocation.getGpsAccuracyStatus());
                    String locationErrorInfo = aMapLocation.getErrorInfo();
                    String locationErrorCode = String.valueOf(aMapLocation.getErrorCode());

                    map.put("lon", lon);
                    map.put("lat", lat);
                    map.put("address", address);
                    mLocationCallBack.callBack(map);
                    return;
                }else {
                    mLocationCallBack.callBack(map);
                }
            }
        });
        AMapLocationClientOption locationOption = getDefaultOption(isHighPrecision);
        // 设置定位模式为低功耗模式
//        locationOption.setLocationMode(AMapLocationMode.Hight_Accuracy);
//        locationOption.setOnceLocation(true);
        mAMapLocationClient.setLocationOption(locationOption);// 设置定位参数
        // 启动定位
        mAMapLocationClient.startLocation();
    }


    /**
     * 设置定位配置
     * @param isHighPrecision
     * @return
     */
    private static AMapLocationClientOption getDefaultOption(boolean isHighPrecision){
        AMapLocationClientOption mOption = new AMapLocationClientOption();
        mOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
        if(isHighPrecision) {
            mOption.setGpsFirst(true);//可选，设置是否gps优先，只在高精度模式下有效。默认关闭
        }else {
            mOption.setGpsFirst(false);
        }
        mOption.setHttpTimeOut(30000);//可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
        mOption.setInterval(2000);//可选，设置定位间隔。默认为2秒
        mOption.setNeedAddress(true);//可选，设置是否返回逆地理地址信息。默认是true
        mOption.setOnceLocation(true);//可选，设置是否单次定位。默认是false
        mOption.setOnceLocationLatest(false);//可选，设置是否等待wifi刷新，默认为false.如果设置为true,会自动变为单次定位，持续定位时不要使用
        AMapLocationClientOption.setLocationProtocol(AMapLocationClientOption.AMapLocationProtocol.HTTP);//可选， 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
        mOption.setSensorEnable(false);//可选，设置是否使用传感器。默认是false
        mOption.setWifiScan(true); //可选，设置是否开启wifi扫描。默认为true，如果设置为false会同时停止主动刷新，停止以后完全依赖于系统刷新，定位位置可能存在误差
        mOption.setLocationCacheEnable(false); //可选，设置是否使用缓存定位，默认为true
        //        mOption.setGeoLanguage(AMapLocationClientOption.GeoLanguage.DEFAULT);//可选，设置逆地理信息的语言，默认值为默认语言（根据所在地区选择语言）
        return mOption;

    }

    /**
     * 定位返回接口
     */
    public interface LocationCallBack {
        void callBack(Map<String, Object> map);
    }

}
