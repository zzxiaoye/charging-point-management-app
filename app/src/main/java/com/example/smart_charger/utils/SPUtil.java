package com.example.smart_charger.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * 轻量级存储
 */
public class SPUtil {

    // fileName
    public static final String SP_FILE_NAME = "smart_charger";
    // 记住密码
    public static final String SP_REMEMBER_ACCOUNT = "rememberAccount";
    // 自动登录
    public static final String SP_AUTOMATIC_LOGIN = "automaticLogin";
    // 账号
    public static final String SP_NUMBER = "number";
    // 昵称
    public static final String SP_NICK_NAME = "nickname";
    // 头像
    public static final String SP_HEADER_PATH = "header_path";
    // 用户id
    public static final String SP_ID = "userid";
    // 密码
    public static final String SP_PASSWORD = "password";
    // 性别
    public static final String SP_SEX = "sex";
    // 使用的充电桩的ID
    public static final String SP_USE_CHARGER_ID = "use_charger_id";
    // 预约的充电桩的ID
    public static final String SP_RESERVE_CHARGER_ID = "reserve_charger_id";
    // 预约时候的预约有效时间
    public static final String SP_RESEVER_TIME = "resever_time";

    /**
     * 保存int类型数据
     */
    public static void createPrefereceInt(String fileName, Context context, String key, int value) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    /**
     * 取int
     */
    public static int getPrefereceFileKeyValueInt(String fileName, Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        int value = prefs.getInt(key, 0);
        return value;
    }


    /**
     * 保存value到文件
     *
     * @param fileName
     *        文件名字
     * @param context
     *        上下文
     * @param key
     *        保存key
     * @param value
     *        对应value
     */
    public static void createPrefereceFile(String fileName, Context context, String key, String value) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    /**
     * 获取文件中的value
     *
     * @param fileName
     *        文件名
     * @param context
     *        上下文
     * @param key
     *        对应key
     * @return value
     */
    public static String getPrefereceFileKeyValue(String fileName, Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        String value = prefs.getString(key, "");
        return value;
    }


    /**
     * 存 boolean
     * @param fileName
     * @param context
     * @param key
     * @param value
     */
    public static void createPrefereceBoolean(String fileName, Context context, String key, boolean value) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }


    /**
     * 取 boolean
     * @param fileName
     * @param context
     * @param key
     * @return
     */
    public static boolean getPrefereceFileKeyBoolean(String fileName, Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences(fileName, Activity.MODE_PRIVATE);
        boolean value = prefs.getBoolean(key, false);
        return value;
    }
}
