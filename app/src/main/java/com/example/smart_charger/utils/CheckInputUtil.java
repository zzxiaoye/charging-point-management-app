package com.example.smart_charger.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckInputUtil {

    public static boolean checkInput(String inputStr) {
        if(inputStr.contains("。")) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]+.[0-9]+");
        Matcher matcher = pattern.matcher((CharSequence) inputStr);
        return matcher.matches();
    }
}
