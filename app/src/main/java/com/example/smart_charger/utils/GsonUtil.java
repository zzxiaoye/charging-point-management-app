package com.example.smart_charger.utils;

import com.google.gson.Gson;

public class GsonUtil {

    private GsonUtil(){}

    private static Gson instance;

    public static Gson getInstance() {
        if(instance == null) {
            instance = new Gson();
        }
        return instance;
    }
}
