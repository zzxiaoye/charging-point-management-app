package com.example.smart_charger.utils;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

import androidx.core.graphics.drawable.DrawableCompat;

/**
 *
 */
public class SettingUtil {

    public static void setColor(TextView textView, int color) {
        textView.setTextColor(color);
        Drawable insertPicture = textView.getCompoundDrawables()[1];
        Drawable drawableDeep = DrawableCompat.wrap(insertPicture.mutate());
        DrawableCompat.setTint(drawableDeep, color);
        textView.setCompoundDrawables(null, drawableDeep, null, null);
    }
}
