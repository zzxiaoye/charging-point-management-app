package com.example.smart_charger.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.smart_charger.App;
import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.databinding.ActivityRegisterBinding;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.utils.GsonUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 注册页
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private ActivityRegisterBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        binding.setViewClick(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                register();
                break;
        }
    }

    private void register() {
        String userName = binding.etUsername.getText().toString();
        String password = binding.etPassword.getText().toString();
        String confirmPassword = binding.etConfirmPassword.getText().toString();
        String nickName = binding.etNickname.getText().toString();
        String sex = binding.etSex.getText().toString();
        if(TextUtils.isEmpty(userName) ||
                TextUtils.isEmpty(password) ||
                TextUtils.isEmpty(confirmPassword) ||
                TextUtils.isEmpty(nickName) ||
                TextUtils.isEmpty(sex)) {
            Toast.makeText(App.context, "请输入完整信息", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!password.equals(confirmPassword)) {
            Toast.makeText(App.context, "两次密码输入不一致", Toast.LENGTH_SHORT).show();
            return;
        }
        binding.btnSubmit.setEnabled(false);
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("number", userName);
        params.put("password", password);
        params.put("nickName", nickName);
        if(sex.equals("女")) {
            params.put("sex", 1);
        }else {
            params.put("sex", 0);
        }
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER);
        OkhttpClient.asyncRequestPostOrPut(this, requestParameter, true, "put", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.btnSubmit.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(App.context, "注册失败：" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.btnSubmit.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Map<String, Object> resultMap = GsonUtil.getInstance().fromJson(responseBody, Map.class);
                double dStatus = (double) resultMap.get("status");
                int status = (int) dStatus;
                if(status == 0) {
                    // 注册成功private
                    Toast.makeText(App.context, "注册成功", Toast.LENGTH_SHORT).show();
                    RegisterActivity.this.finish();
                }else {
                    Toast.makeText(App.context, (String) resultMap.get("msg"), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
