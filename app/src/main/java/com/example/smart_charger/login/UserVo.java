package com.example.smart_charger.login;

import com.example.smart_charger.dto.BaseDTO;

/**
 */
public class UserVo extends BaseDTO<UserVo> {

    // 用户ID
    private String id;
    // 用户账号
    private String number;
    // 用户密码
    private String password;
    // 用户新密码
    private String newPassword;
    // 用户昵称(如果数据库的字段名是：nick_name, 那么pojo包下实体类里面的属性要遵守驼峰命名法则)
    private String nickName;
    // 头像
    private String headerPath;
    // 性别： 0 男  1 女
    private int sex;
    // 充电桩的id
    private String chargerId;
    // 是否惩罚 0: 正常， 1：惩罚（不能预约和使用）
    private int isPenalty;

    // 惩罚结束时间
    private String penaltyEndTime;

    public String getPenaltyEndTime() {
        return penaltyEndTime;
    }

    public void setPenaltyEndTime(String penaltyEndTime) {
        this.penaltyEndTime = penaltyEndTime;
    }

    public int getIsPenalty() {
        return isPenalty;
    }

    public void setIsPenalty(int isPenalty) {
        this.isPenalty = isPenalty;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeaderPath() {
        return headerPath;
    }

    public void setHeaderPath(String headerPath) {
        this.headerPath = headerPath;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getChargerId() {
        return chargerId;
    }

    public void setChargerId(String chargerId) {
        this.chargerId = chargerId;
    }
}
