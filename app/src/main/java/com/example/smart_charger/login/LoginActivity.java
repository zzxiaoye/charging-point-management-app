package com.example.smart_charger.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.databinding.ActivityLoginBinding;
import com.example.smart_charger.dto.BaseDTO;
import com.example.smart_charger.home.HomeActivity;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.utils.GsonUtil;
import com.example.smart_charger.utils.SPUtil;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setViewClick(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                toLogin();
                break;
            case R.id.tv_register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }


    private void toLogin() {
        String userName = binding.etUsername.getText().toString();
        String password = binding.etPassword.getText().toString();
        if(TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "请输入完整信息", Toast.LENGTH_SHORT).show();
            return;
        }
        binding.btnLogin.setEnabled(false);
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("number", userName);
        params.put("password", password);
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER);
        OkhttpClient.asyncRequestPostOrPut(this, requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.btnLogin.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, "登录失败：" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.btnLogin.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    // 注册成功
                    Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                    // 保存账号信息
                    UserVo mUserVo = dto.getData().get(0);
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_NUMBER, mUserVo.getNumber());
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_NICK_NAME, mUserVo.getNickName());
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_ID, mUserVo.getId());
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_PASSWORD, mUserVo.getPassword());
                    SPUtil.createPrefereceInt(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_SEX, mUserVo.getSex());
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_HEADER_PATH, mUserVo.getHeaderPath());
//                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_USE_CHARGER_ID, mUserVo.getUseChargerId());
//                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, LoginActivity.this, SPUtil.SP_RESERVE_CHARGER_ID, mUserVo.getReserveChargerId());
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }else {
                    Toast.makeText(LoginActivity.this, "登录失败：" + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
