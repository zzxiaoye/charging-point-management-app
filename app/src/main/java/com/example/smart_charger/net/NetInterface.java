package com.example.smart_charger.net;

/**
 * 接口地址
 */
public class NetInterface {

    // 更改密码
    public static String USER_CHANGE_PAW = "/user/changepwd";
    // 注册/登录
    public static String USER = "/user";
    // 充电桩列表
    public static String CHARGE = "/charger";
    // 预约某个充电桩
    public static String RESEVER_CHARGE = "/reserve/charger";
    // 取消预约
    public static String RESEVER_CANCLE_CHARGE = "/reserve/cancel/charger";
    // 获取某给充电桩信息
    public static String CHARGE_ITEM = "/charger/item";

    // 开始使用某个充电桩
    public static String START_CHARGER = "/start/charger";
    // 结束充电
    public static String END_CHARGER = "/end/charger";

    // 上传头像
    public static final String UPLOAD_FILE = "/file";

}
