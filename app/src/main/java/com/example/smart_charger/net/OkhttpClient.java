package com.example.smart_charger.net;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * 网络相关
 */
public class OkhttpClient {

    public static OkHttpClient getOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    /**
     * 同步post, put请求
     * @param context
     * @param parameter
     * @param runOnUiThread
     * @param requestType
     * @param callback
     */
    public static void syncRequestPostOrPut(final Context context, RequestParameter parameter, final boolean runOnUiThread, String requestType, final RequestCallback callback) {
        String url = parameter.getUrl();
        String body = parameter.getBody();
        MediaType mediaType = parameter.getMediaType();
        RequestBody requestBody = RequestBody.create(mediaType, body);
        Request.Builder requestBuilder = new Request.Builder();
        Map<String, String> mapHeader = parameter.getHeaderMap();
        for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
            requestBuilder.addHeader(entry.getKey(), entry.getValue());
        }
        if(TextUtils.isEmpty(requestType)) {
            return;
        }
        if(requestType.equals("post")) {
            requestBuilder.post(requestBody);
        }else if(requestType.equals("put")) {
            requestBuilder.put(requestBody);
        }else {
            return;
        }
        final Request request = requestBuilder.url(url).build();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = getOkHttpClient().newCall(request).execute();
                    final String code = String.valueOf(response.code());
                    if(code.equals("200")) {
                        final Response finalResponse = response;
                        final String responseBody = finalResponse.body().string();
                        final String responseHeader = finalResponse.headers().toString();
                        final Map<String, String> map = new HashMap<>();
                        map.put("responseBody", responseBody);
                        map.put("responseHeader", responseHeader);
                        if(runOnUiThread) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onSuccess(map);
                                }
                            });
                        }else {
                            callback.onSuccess(map);
                        }
                    }else {
                        if(runOnUiThread) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onFailure(code);
                                }
                            });
                        }else {
                            callback.onFailure(code);
                        }
                    }
                } catch (final IOException e) {
                    e.printStackTrace();
                    if(runOnUiThread) {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onFailure(e.toString());
                            }
                        });
                    }else {
                        callback.onFailure(e.toString());
                    }

                }
            }
        }).start();
    }

    /**
     * 异步Post, Put请求
     * @param context
     * @param parameter
     * @param runOnUiThread
     * @param requestType
     * @param callback
     */
    public static void asyncRequestPostOrPut(final Context context, RequestParameter parameter, final boolean runOnUiThread, String requestType, final RequestCallback callback) {
        String url = parameter.getUrl();
        String body = parameter.getBody();
        MediaType mediaType = parameter.getMediaType();
        final RequestBody requestBody = RequestBody.create(mediaType, body);
        Request.Builder requestBuilder = new Request.Builder();
        Map<String, String> mapHeader = parameter.getHeaderMap();
        for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
            requestBuilder.addHeader(entry.getKey(), entry.getValue());
        }
        if(TextUtils.isEmpty(requestType)) {
            return;
        }
        if(requestType.equals("post")) {
            requestBuilder.post(requestBody);
        }else if(requestType.equals("put")) {
            requestBuilder.put(requestBody);
        }else {
            return;
        }
        final Request request = requestBuilder.url(url).build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e.toString());
                        }
                    });
                }else {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                String responseHeader = response.headers().toString();
                int code = response.code();
                final Map<String, String> map = new HashMap<>();
                map.put("code", code + "");
                if(responseBody != null) {
                    map.put("responseBody", responseBody);
                }
                if(responseHeader != null) {
                    map.put("responseHeader", responseHeader);
                }
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(map);
                        }
                    });
                }else {
                    callback.onSuccess(map);
                }
            }
        });
    }

    /**
     * 同步Get请求
     * @param context
     * @param parameter
     * @param runOnUiThread
     * @param callback
     */
    public static void syncRequestGet(final Context context, RequestParameter parameter, final boolean runOnUiThread, final RequestCallback callback) {
        Request.Builder requestBuilder = new Request.Builder();
        Map<String, String> mapHeader = parameter.getHeaderMap();
        if(mapHeader != null) {
            for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        requestBuilder.url(parameter.getUrl());
        final Request request = requestBuilder.build();
        final Call call = getOkHttpClient().newCall(request);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = call.execute();
                    final String code = String.valueOf(response.code());
                    if(code.equals("200")) {
                        final Response finalResponse = response;
                        final String responseBody = finalResponse.body().string();
                        final String responseHeader = finalResponse.headers().toString();
                        final Map<String, String> map = new HashMap<>();
                        map.put("responseBody", responseBody);
                        map.put("responseHeader", responseHeader);
                        if(runOnUiThread) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onSuccess(map);
                                }
                            });
                        }else {
                            callback.onSuccess(map);
                        }
                    }else {
                        if(runOnUiThread) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onFailure(code);
                                }
                            });
                        }else {
                            callback.onFailure(code);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    callback.onFailure(e.toString());
                }
            }
        }).start();
    }

    /**
     * 异步Get请求
     * @param context
     * @param parameter
     * @param runOnUiThread
     * @param callback
     */
    public static void asyncRequestGet(final Context context, RequestParameter parameter, final boolean runOnUiThread, final RequestCallback callback) {
        Request.Builder requestBuilder = new Request.Builder();
        Map<String, String> mapHeader = parameter.getHeaderMap();
        if(mapHeader != null) {
            for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        requestBuilder.url(parameter.getUrl());
        final Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e.toString());
                        }
                    });
                }else {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                String responseHeader = response.headers().toString();
                final Map<String, String> map = new HashMap<>();
                if(responseBody != null) {
                    map.put("responseBody", responseBody);
                }
                if(responseHeader != null) {
                    map.put("responseHeader", responseHeader);
                }
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(map);
                        }
                    });
                }else {
                    callback.onSuccess(map);
                }
            }
        });
    }

    /**
     * 获取手机验证码
     * @param phone
     * @param type
     */
    public static void getVerificationCode(final Context context, String phone, String type, final boolean runOnUiThread, final RequestCallback callback) {
//        if(TextUtils.isEmpty(phone)) {
//            return;
//        }
//        if(TextUtils.isEmpty(type)) {
//            return;
//        }
//        Map<String, String> bodyMap = new HashMap<>();
//        bodyMap.put("mobile_no", phone);
//        bodyMap.put("function_type", type);
//        Gson gson = new Gson();
//        String body = gson.toJson(bodyMap, Map.class);
//        MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");
//        final RequestBody requestBody = RequestBody.create(mediaType, body);
//        Request.Builder requestBuilder = new Request.Builder();
//        requestBuilder.post(requestBody);
//        requestBuilder.addHeader("X-RSA-KID", "d94ffbb57a654fb1997bec3a85b07c2d");
//        requestBuilder.addHeader("Content-Type", "application/json");
//        String url = NetInterface.ZWT_BASE_URL + NetInterface.GET_VERIFY_NO;
//        final Request request = requestBuilder.url(url).build();
//        Call call = getOkHttpClient().newCall(request);
//        call.enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, final IOException e) {
//                Log.d("Activity", "短信验证码 error :" + e.toString());
//                if(runOnUiThread) {
//                    ((Activity)context).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            callback.onFailure(e.toString());
//                        }
//                    });
//                }else {
//                    callback.onFailure(e.toString());
//                }
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                String responseBody = response.body().string();
//                String responseHeader = response.headers().toString();
//                Log.d("Activity", "短信验证码 responseBody :" + responseBody);
//                final Map<String, String> map = new HashMap<>();
//                if(responseBody != null) {
//                    map.put("responseBody", responseBody);
//                }
//                if(responseHeader != null) {
//                    map.put("responseHeader", responseHeader);
//                }
//                if(runOnUiThread) {
//                    ((Activity)context).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            callback.onSuccess(map);
//                        }
//                    });
//                }else {
//                    callback.onSuccess(map);
//                }
//            }
//        });
    }

    /**
     * 上传文件，无进度回调
     * @param fileList
     * @param parameter
     * @param callback
     */
    public static void uploadFile(final Context context, List<File> fileList, RequestParameter parameter, final boolean runOnUiThread, final RequestCallback callback) {
        MultipartBody.Builder mMultipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for(int i = 0; i < fileList.size(); i++) {
            mMultipartBody.addFormDataPart("file", fileList.get(i).getName(), RequestBody.create(MediaType.parse("multipart/form-data"), fileList.get(i)));
        }
        // 其他数据型参数
        Map<String, String> bodyMap = parameter.getBodyMap();
        if(bodyMap != null) {
            for(Map.Entry<String, String> entry : bodyMap.entrySet()) {
                mMultipartBody.addFormDataPart(entry.getKey(), entry.getValue());
            }
        }

        RequestBody mRequestBody = mMultipartBody.build();
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(parameter.getUrl());
        requestBuilder.post(mRequestBody);
        Map<String, String> mapHeader = parameter.getHeaderMap();
        if(mapHeader != null) {
            for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e.toString());
                        }
                    });
                }else {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseBody = response.body().string();
                String responseHeader = response.headers().toString();
                final Map<String, String> map = new HashMap<>();
                if(responseBody != null) {
                    map.put("responseBody", responseBody);
                }
                if(responseHeader != null) {
                    map.put("responseHeader", responseHeader);
                }
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(map);
                        }
                    });
                }else {
                    callback.onSuccess(map);
                }
            }
        });
    }


    /**
     * 真正的上传文件
     * @param context
     * @param file
     * @param parameter
     * @param runOnUiThread
     * @param progressListener
     */
    public static void uploadFIleProgressS3(final Context context, File file, RequestParameter parameter, final  boolean runOnUiThread, final ProgressListener progressListener) {
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(parameter.getUrl());

        Map<String, String> headerMap = parameter.getHeaderMap();
        if(headerMap != null && headerMap.size() > 0) {
            for(Map.Entry<String, String> entry : headerMap.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }

//        MultipartBody.Builder mMultipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        MultipartBody.Builder mMultipartBody = new MultipartBody.Builder();
//        mMultipartBody.addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("application/octet-stream"), file));
//        RequestBody requestBody = RequestBody.create()
        RequestBody mRequestBody = mMultipartBody.build();
        ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, mRequestBody, progressListener);

        requestBuilder.put(progressRequestBody);
        Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressListener.onFail(e.toString());
                        }
                    });
                }else {
                    progressListener.onFail(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                Log.d("Activity", "OkhttpClient  responseBody == " + responseBody);
            }
        });
    }

    /**
     * 上传 有进度回调
     * @param context
     * @param file
     * @param parameter
     * @param runOnUiThread
     * @param progressListener
     */
    public static void uploadFileProgress(final Context context, File file, RequestParameter parameter, final boolean runOnUiThread, final ProgressListener progressListener) {
        MultipartBody.Builder mMultipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        mMultipartBody.addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        Map<String, String> bodyMap = parameter.getBodyMap();
        if(bodyMap != null && bodyMap.size() > 0) {
            for(Map.Entry<String, String> entry : bodyMap.entrySet()) {
                mMultipartBody.addFormDataPart(entry.getKey(), entry.getValue());
            }
        }
        RequestBody mRequestBody = mMultipartBody.build();
        // 实例化ProgressRequestBody
        ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, mRequestBody, progressListener);
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(parameter.getUrl());
        requestBuilder.post(progressRequestBody);
        Map<String, String> mapHeader = parameter.getHeaderMap();
        for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
            requestBuilder.addHeader(entry.getKey(), entry.getValue());
        }
        Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressListener.onFail(e.toString());
                        }
                    });
                }else {
                    progressListener.onFail(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String responseBody = response.body().string();
                Log.d("Activity", "OkhttpClient  responseBody == " + responseBody);
                // 成功返回["d969ebe4433f40c994c55bbaa857b845"]
                if(!TextUtils.isEmpty(responseBody) && responseBody.startsWith("[") && responseBody.endsWith("]")) {
                    if(runOnUiThread) {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressListener.onFinish(responseBody);
                            }
                        });
                    }else {
                        progressListener.onFinish(responseBody);
                    }
                }else {
                    if(runOnUiThread) {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressListener.onFail(responseBody);
                            }
                        });
                    }else {
                        progressListener.onFail(responseBody);
                    }
                }
            }
        });
    }

    /**
     * 下载文件
     * @param context
     * @param parentFile
     * @param targetFileName
     * @param parameter
     * @param runOnUiThread
     * @param callback
     */
    public static void downloadFileHttp(final Context context, File parentFile, String targetFileName, RequestParameter parameter, final boolean runOnUiThread, final RequestCallback callback) {
        try {
            URL url = new URL(parameter.getUrl());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //设置超时间为3秒
            conn.setConnectTimeout(3 * 1000);
            //防止屏蔽程序抓取而返回403错误
//            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            Map<String, String> mapHeader = parameter.getHeaderMap();
            if(mapHeader != null) {
                for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            conn.addRequestProperty("Accept-Encoding", "identity");
            //得到输入流
            InputStream inputStream = conn.getInputStream();
            //totalSize变量用来获得将要下载文件的总大小
            int totalSize = conn.getContentLength();
            Log.d("Activity", "文件总大小 == " + totalSize);
            if(!parentFile.exists()) {
                parentFile.mkdirs();
            }
            File targetFile = new File(parentFile, targetFileName);
            saveFileNew(targetFile, inputStream, totalSize);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 保存文件
     * @param targetFile
     * @param inputStream
     * @param totalSize
     */
    private static void saveFileNew(File targetFile, InputStream inputStream, int totalSize) {
        int currentSize = 0;
        try {
            OutputStream os = new FileOutputStream(targetFile);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
                currentSize += bytesRead;
                int progressSize = (int)(currentSize/(double)totalSize * 100);
                Log.d("Activity", "进度 ============= " + progressSize);
            }
            os.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载
     * @param parentFile  下载后放在哪里
     * @param targetFileName  目标文件下载后的文件命名
     * @param parameter
     * @param progressListener
     */
    public static void downloadFile(final Context context, File parentFile, String targetFileName, RequestParameter parameter, final boolean runOnUiThread, final ProgressListener progressListener) {
        if(!parentFile.exists()) {
            parentFile.mkdirs();
        }
        // 目标文件
        final File targetFile = new File(parentFile, targetFileName);
        Request.Builder requestBuilder = new Request.Builder();
        Map<String, String> mapHeader = parameter.getHeaderMap();
        if(mapHeader != null) {
            for(Map.Entry<String, String> entry : mapHeader.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        requestBuilder.addHeader("Accept-Encoding", "identity");
        requestBuilder.url(parameter.getUrl());
        Request request = requestBuilder.build();
        Call call = getOkHttpClient().newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                if(runOnUiThread) {
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressListener.onFail(e.toString());
                        }
                    });
                }else {
                    progressListener.onFail(e.toString());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.code() == 200) {
                    ResponseBody body = response.body();
                    // 获取文件总长度
                    final long totalLength = body.contentLength();
//                    Log.d("Activity", "总大小 ==== " + totalLength);
                    //以流的方式进行读取
                    try {
                        InputStream inputStream = body.byteStream();
                        FileOutputStream outputStream = new FileOutputStream(targetFile);
                        byte[] buffer = new byte[2048];
                        int len = 0;
                        int num = 0;
                        while ((len = inputStream.read(buffer)) != -1){
                            num += len;
//                            Log.d("MainActivity", "okhttp  累计下载了大小 == " + num);
                            outputStream.write(buffer,0,len);
//                            final int finalNum = num;
//                            Log.d("MainActivity", "okhttp  下载进度 == " + num * 100 / totalLength);
                            if(runOnUiThread) {
                                final int finalNum = num;
                                ((Activity)context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Log.d("MainActivity", "finalNum == " + finalNum);
                                        progressListener.onProgress(finalNum, totalLength);
                                    }
                                });
                            }else {
                                progressListener.onProgress(num, totalLength);
                            }
                        }
                        //读取完关闭流
                        outputStream.flush();
                        if(inputStream != null) {
                            inputStream.close();
                        }
                        if(outputStream != null) {
                            outputStream.close();
                        }
                        if(runOnUiThread) {
                            ((Activity)context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressListener.onFinish(null);
                                }
                            });
                        }else {
                            progressListener.onFinish(null);
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                        Log.d("Activity", "error == " + e.toString());
                    }
                }else {
                    if(runOnUiThread) {
                        ((Activity)context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressListener.onFail("下载失败");
                            }
                        });
                    }else {
                        progressListener.onFail("下载失败");
                    }
                }

            }
        });
    }
}
