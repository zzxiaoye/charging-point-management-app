package com.example.smart_charger.net;

/**
 * Describe: 进度条
 */
public interface ProgressListener {

    /**
     * 进度回调
     * @param currentBytes    累计的大小
     * @param contentLength   文件的总大小
     */
    void onProgress(long currentBytes, long contentLength);
    /**
     * 上传完成
     */
    void onFinish(String pkid);

    /**
     * 上传失败
     */
    void onFail(String msg);
}
