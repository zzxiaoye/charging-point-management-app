package com.example.smart_charger.net;


import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

/**
 * Describe: 上传文件带进度的
 */
public class ProgressRequestBody extends RequestBody {

    private RequestBody requestBody;
    private ProgressListener progressListener;
    private File targetFile;
    private BufferedSink bufferedSink;

    public ProgressRequestBody(File file, RequestBody body, ProgressListener listener) {
        requestBody = body;
        progressListener = listener;
        targetFile = file;
    }

    @Nullable
    @Override
    public MediaType contentType() {
        return requestBody.contentType();
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        if (bufferedSink==null){
            bufferedSink = Okio.buffer(sink(sink));
        }
        //写入
        requestBody.writeTo(bufferedSink);
        //刷新
        bufferedSink.flush();
    }

    private Sink sink(BufferedSink sink) {
        return new ForwardingSink(sink) {
            long bytesWritten = 0L;
            long contentLength = 0L;
            @Override
            public void write(Buffer source, long byteCount) throws IOException {
                super.write(source, byteCount);
                if (contentLength==0){
//                    contentLength = contentLength();
                    contentLength = targetFile.length();
                }
                bytesWritten += byteCount;
//                // 上传完成
//                if(bytesWritten == contentLength) {
//                    progressListener.onFinish();
//                    return;
//                }
                progressListener.onProgress(bytesWritten, contentLength);
            }
        };
    }
}
