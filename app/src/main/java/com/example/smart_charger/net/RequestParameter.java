package com.example.smart_charger.net;

import java.io.Serializable;
import java.util.Map;

import okhttp3.MediaType;

/**
 * Describe:  请求参数，包括url, body, header
 */
public class RequestParameter implements Serializable {

    private String url;
    private String body;
    private MediaType mediaType;
    private Map<String, String> headerMap;
    private Map<String, String> bodyMap;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Map<String, String> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<String, String> headerMap) {
        this.headerMap = headerMap;
    }

    public Map<String, String> getBodyMap() {
        return bodyMap;
    }

    public void setBodyMap(Map<String, String> bodyMap) {
        this.bodyMap = bodyMap;
    }
}
