package com.example.smart_charger.net;

import java.util.Map;

/**
 * 网络请求回调
 */
public interface RequestCallback {

    void onFailure(String msg);
    void onSuccess(Map<String, String> map);
}
