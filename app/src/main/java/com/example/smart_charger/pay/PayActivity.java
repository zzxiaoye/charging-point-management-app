package com.example.smart_charger.pay;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.smart_charger.R;
import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.charger.ChargerVo;
import com.example.smart_charger.databinding.ActivityPayBinding;
import com.example.smart_charger.view.InputDialog;
import com.example.smart_charger.view.PayDialog;

public class PayActivity extends BaseActivity implements View.OnClickListener {

    private ActivityPayBinding binding;

    private PayDialog mPayDialog;

    private String payMoney;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pay);
        binding.setViewClick(this);
        init();
    }

    private void init() {
        payMoney = getIntent().getStringExtra("payMoney");
        if(!TextUtils.isEmpty(payMoney)) {
            binding.tvPayMoney.setText("￥" + payMoney);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_pay:
                showPayDialog(payMoney);
                break;
            case R.id.iv_back:
                this.finish();
                break;
        }
    }


    private void showPayDialog(String payMoney) {
        if(mPayDialog == null) {
            mPayDialog = new PayDialog(this);
        }
        mPayDialog.setPayOnClickListener(new PayDialog.PayOnClickListener() {
            @Override
            public void payOnClick(String payMoney) {
                toPay(payMoney);
            }
        });
        mPayDialog.show();
        mPayDialog.setPayMoney(payMoney);
        mPayDialog.setCancelable(false);
    }


    private void toPay(String payMoney) {
        setResult(99);
        finish();
    }


}
