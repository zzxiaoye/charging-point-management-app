package com.example.smart_charger.mine;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.example.smart_charger.App;
import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.databinding.FragmentMineBinding;
import com.example.smart_charger.dto.BaseDTO;
import com.example.smart_charger.home.ChangePwdActivity;
import com.example.smart_charger.login.UserVo;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.utils.GsonUtil;
import com.example.smart_charger.utils.SPUtil;
import com.google.gson.reflect.TypeToken;
import com.lcw.library.imagepicker.ImagePicker;
import com.lcw.library.imagepicker.utils.ImageLoader;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 */
public class MineFragment extends Fragment implements View.OnClickListener {

    private FragmentMineBinding binding;

    public static final int REQUEST_SELECT_IMAGES_CODE = 0x01;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mine,container,false);
        binding.setViewClick(this);
        init();
        return binding.getRoot();
    }

    private void init() {
        String nickName = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_NICK_NAME);
        if(!TextUtils.isEmpty(nickName)) {
            binding.tvNicknameView.setText(nickName);
        }
        String headImgUrl = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_HEADER_PATH);
        if(!TextUtils.isEmpty(headImgUrl)) {
            Glide.with(getActivity()).load(Config.BASE_URL + File.separator + "file?fileName=" + headImgUrl).into(binding.oivHeaderView);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.oiv_header_view:
                setHeaderImg();
                break;
            case R.id.tv_logout:
                logout();
                break;
            case R.id.mcv_change_pwd_view:
                startActivity(new Intent(getActivity(), ChangePwdActivity.class));
                break;
        }
    }




    private void logout() {
        SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID, "");
        SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_NICK_NAME, "");
        SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_HEADER_PATH, "");
        SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_NUMBER, "");
        SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_PASSWORD, "");
        SPUtil.createPrefereceInt(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_SEX, 0);
        Toast.makeText(App.context, "退出登录成功", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    private void setHeaderImg() {
        ImagePicker.getInstance()
                .setTitle("标题")//设置标题
                .showCamera(true)//设置是否显示拍照按钮
                .showImage(true)//设置是否展示图片
                .showVideo(true)//设置是否展示视频
                .setSingleType(true)//设置图片视频不能同时选择
                .setImageLoader(new GlideLoader())//设置自定义图片加载器
                .start(getActivity(), REQUEST_SELECT_IMAGES_CODE);//REQEST_SELECT_IMAGES_CODE为Intent调用的requestCode
    }


    public class GlideLoader implements ImageLoader {

        private RequestOptions mOptions = new RequestOptions()
                .centerCrop()
                .format(DecodeFormat.PREFER_RGB_565)
                .placeholder(R.drawable.ic_header)
                .error(R.drawable.ic_header);

        private RequestOptions mPreOptions = new RequestOptions()
                .skipMemoryCache(true)
                .error(R.drawable.ic_header);

        @Override
        public void loadImage(ImageView imageView, String imagePath) {
            //小图加载
            Glide.with(imageView.getContext()).load(imagePath).apply(mOptions).into(imageView);
        }

        @Override
        public void loadPreImage(ImageView imageView, String imagePath) {
            //大图加载
            Glide.with(imageView.getContext()).load(imagePath).apply(mPreOptions).into(imageView);

        }

        @Override
        public void clearMemoryCache() {
            //清理缓存
            Glide.get(App.context).clearMemory();
        }
    }


    public void showHeaderImg(String imgPath) {
        if(!TextUtils.isEmpty(imgPath)) {
//            Glide.with(getActivity()).load(imgPath).into(binding.oivHeaderView);
            // 上传头像
            uploadHeader(imgPath);
        }
    }


    private void uploadHeader(String imgPath) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, String> params = new HashMap<>();
        params.put("userId", SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, App.context, SPUtil.SP_ID));
        requestParameter.setBodyMap(params);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.UPLOAD_FILE);
        List<File> fileList = new ArrayList<>();
        fileList.add(new File(imgPath));
        OkhttpClient.uploadFile(getActivity(), fileList, requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Log.e("zgylog", "msg == " + msg);
                Toast.makeText(getActivity(), "上传头像失败: " + msg, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Log.e("zgylog", "responseBody == " + responseBody);
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    String path = Config.BASE_URL + File.separator + "file?fileName=" + dto.getData().get(0).getHeaderPath();
                    // 保存图片的网络地址
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, App.context, SPUtil.SP_HEADER_PATH, dto.getData().get(0).getHeaderPath());
                    Log.e("zgylog", "path == " + path);
                    Glide.with(getActivity()).load(path).into(binding.oivHeaderView);
                }else {
                    Toast.makeText(App.context, "上传头像失败：" + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
