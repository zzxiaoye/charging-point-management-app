package com.example.smart_charger.manager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.smart_charger.R;
import com.example.smart_charger.databinding.FragmentManagerBinding;

/**
 */
public class ManagerFragment extends Fragment implements View.OnClickListener {

    private FragmentManagerBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manager, container, false);
        binding.setViewClick(this);
        init();
        return binding.getRoot();
    }

    private void init() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_user_title:
                binding.rlQueryUserLayout.setVisibility(View.VISIBLE);
                binding.rlQueryChargerLayout.setVisibility(View.GONE);
                break;
            case R.id.tv_charger_title:
                binding.rlQueryUserLayout.setVisibility(View.GONE);
                binding.rlQueryChargerLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.but_query_user:
                queryUser();
                break;
            case R.id.but_query_charger:
                queryCharger();
                break;
            case R.id.but_change_charger_time:
                changeChargerTime();
                break;
        }
    }


    private void queryUser() {

    }

    private void queryCharger() {

    }

    private void changeChargerTime() {

    }
}
