package com.example.smart_charger;

/**
 * 配置类
 */
public class Config {

    // 0 ： 普通用户   1 ： 管理员
    public static final int type = 0;
    public static final String BASE_URL = "http://192.168.198.156:8080";
//    public static final String BASE_URL = "http://ccqmah9x.dongtaiyuming.net:13086";
    // 花生壳： https://blog.csdn.net/niaonao/article/details/112725465
//    public static final String BASE_URL = "http://3n9b914626.qicp.vip";

    // 预约有效时间  单位：分钟
    public static final int reserveTimeMinute = 1;
}
