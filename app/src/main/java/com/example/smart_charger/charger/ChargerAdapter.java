package com.example.smart_charger.charger;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smart_charger.R;
import com.example.smart_charger.utils.SPUtil;
import com.example.smart_charger.utils.TimeFormatUtil;

import java.util.List;

/**
 */
public class ChargerAdapter extends RecyclerView.Adapter<ChargerAdapter.ViewHolder> {

    private List<ChargerVo> list;
    private Context mContext;
    private LayoutInflater mInflater;

    private ChargerItemOnClickListener mChargerItemOnClickListener;

    public ChargerAdapter(Context mContext, List<ChargerVo> list) {
        this.mContext = mContext;
        this.list = list;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_charger, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChargerVo been = list.get(position);
        if(!TextUtils.isEmpty(been.getName())) {
            holder.tv_charging_name.setText(been.getName());
        }
        if(!TextUtils.isEmpty(been.getLocation())) {
            holder.tv_location.setText(been.getLocation());
        }
        if(!TextUtils.isEmpty(been.getDistance())) {
            holder.tv_distance.setText("距您" + been.getDistance() + "公里");
        }
        // 状态
        int status = been.getUseStatus();
        if(status == 0) {
            holder.tv_status.setText("空闲中");
            holder.tv_time.setText("随时可预约");
            holder.but_reserve.setVisibility(View.VISIBLE);
            holder.but_scan.setVisibility(View.VISIBLE);
        }else if(status ==1) {
            holder.tv_status.setText("预约中");
            long reserveEndTime = System.currentTimeMillis() + been.getReserveEndTime() * 1000;
//            holder.tv_time.setText("预约有效时间：" + TimeFormatUtil.getTime(reserveEndTime));
            holder.tv_time.setText("预约有效时间：" + SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, mContext, SPUtil.SP_RESEVER_TIME));
            holder.but_reserve.setVisibility(View.GONE);
            holder.but_scan.setVisibility(View.GONE);
        }else if(status ==2) {
            holder.tv_status.setText("使用中");
            long chargerEndTime = System.currentTimeMillis() + been.getReserveEndTime() * 1000;
            holder.tv_time.setText("使用结束时间：" + TimeFormatUtil.getTime(chargerEndTime));
            holder.but_reserve.setVisibility(View.GONE);
            holder.but_scan.setVisibility(View.GONE);
        }else if(status ==3) {
            holder.tv_status.setText("充电完成，等待用户确认即可恢复空闲状态");
            holder.tv_time.setText("");
            holder.but_reserve.setVisibility(View.GONE);
            holder.but_scan.setVisibility(View.GONE);
        }

        holder.but_reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != mChargerItemOnClickListener) {
                    mChargerItemOnClickListener.onReserveClick(position);
                }
            }
        });
        holder.but_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != mChargerItemOnClickListener) {
                    mChargerItemOnClickListener.onScanClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_charging_name;
        private TextView tv_location;
        private TextView tv_distance;
        private TextView tv_status;
        private TextView tv_time;
        private Button but_reserve;
        private Button but_scan;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_charging_name = itemView.findViewById(R.id.tv_charging_name);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_distance = itemView.findViewById(R.id.tv_distance);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_time = itemView.findViewById(R.id.tv_time);
            but_reserve = itemView.findViewById(R.id.but_reserve);
            but_scan = itemView.findViewById(R.id.but_scan);
        }
    }

    public interface ChargerItemOnClickListener {
        void onReserveClick(int position);
        void onScanClick(int position);
    }

    public void setChargerItemOnClickListener(ChargerItemOnClickListener mChargerItemOnClickListener) {
        this.mChargerItemOnClickListener = mChargerItemOnClickListener;
    }
}
