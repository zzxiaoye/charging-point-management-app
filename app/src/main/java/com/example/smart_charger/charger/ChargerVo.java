package com.example.smart_charger.charger;

import com.example.smart_charger.dto.BaseDTO;

/**
 */
public class ChargerVo extends BaseDTO<ChargerVo> {

    private String id;
    private String name;
    private String location;
    private String distance;
    // 是否使用: 0 没有使用, 没有预约    1 没有使用，但是有预约    2 正在使用   3 充电完成
    private int useStatus;
    // 还有多少秒充电结束
    private int chargerEndTime;
    // 还有多少秒预约时间失效
    private int reserveEndTime;
    // 当前用户
    private String userId;
    // 当前使用者昵称
    private String userNickName;
    private int belong;
    // 二维码
    private String scanCode;

    public String getScanCode() {
        return scanCode;
    }

    public void setScanCode(String scanCode) {
        this.scanCode = scanCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getUseStatus() {
        return useStatus;
    }

    public void setUseStatus(int useStatus) {
        this.useStatus = useStatus;
    }

    public int getChargerEndTime() {
        return chargerEndTime;
    }

    public void setChargerEndTime(int chargerEndTime) {
        this.chargerEndTime = chargerEndTime;
    }

    public int getReserveEndTime() {
        return reserveEndTime;
    }

    public void setReserveEndTime(int reserveEndTime) {
        this.reserveEndTime = reserveEndTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public int getBelong() {
        return belong;
    }

    public void setBelong(int belong) {
        this.belong = belong;
    }
}
