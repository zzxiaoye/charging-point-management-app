package com.example.smart_charger.charger;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.databinding.FragmentChargerBinding;
import com.example.smart_charger.dto.BaseDTO;
import com.example.smart_charger.home.HomeActivity;
import com.example.smart_charger.login.LoginActivity;
import com.example.smart_charger.login.UserVo;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.utils.GsonUtil;
import com.example.smart_charger.utils.MapLocationUtil;
import com.example.smart_charger.utils.SPUtil;
import com.example.smart_charger.utils.SpacesItemDecoration;
import com.example.smart_charger.utils.TimeFormatUtil;
import com.example.smart_charger.view.InputDialog;
import com.google.gson.reflect.TypeToken;
import com.scan.CaptureActivity;
import com.scan.ScanResultCallback;
import com.scan.bean.ZxingConfig;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public class ChargerFragment extends Fragment implements View.OnClickListener {

    private FragmentChargerBinding binding;
    private ChargerAdapter adapter;
    private List<ChargerVo> list = new ArrayList<>();
    // 请求数据的类型：柳园，荷园，菊园，松园，分别对应0，1，2，3，默认是0
    private int requestType;

    private InputDialog inputDialog;

    private int currentPosition;

    private String inputTime;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_charger,container,false);
        binding.setViewClick(this);
        init();
        return binding.getRoot();
    }

    private void init() {
        initScanCallback();
        adapter = new ChargerAdapter(getActivity(), list);
        adapter.setChargerItemOnClickListener(chargerItemOnClickListener);
        binding.rvRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvRecyclerView.setAdapter(adapter);
        binding.rvRecyclerView.addItemDecoration(new SpacesItemDecoration(20));
        binding.srlRefreshLayout.setRefreshHeader(new ClassicsHeader(getActivity()));
        binding.srlRefreshLayout.setRefreshFooter(new ClassicsFooter(getActivity()));

        binding.srlRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                getChargerList();
            }
        });

        binding.srlRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                getChargerList();
            }
        });
        getChargerList();

        initLocation();
    }



    public void startCharger(String inputTime) {
        if(TextUtils.isEmpty(inputTime)) {
            Toast.makeText(getActivity(), "请输入充电时长", Toast.LENGTH_SHORT).show();
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("id", HomeActivity.currentChargerVo.getId());
        params.put("userId", SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID));
        Log.e("TAG", "inputTime == " + inputTime);
        params.put("chargerEndTime", 60 * Integer.parseInt(inputTime));
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.START_CHARGER);
        OkhttpClient.asyncRequestPostOrPut(getActivity(), requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    Toast.makeText(getActivity(), "已经开始充电", Toast.LENGTH_SHORT).show();
                    ChargerVo refreshChargerVo = (ChargerVo) dto.getData().get(0);
                    list.get(currentPosition).setUseStatus(refreshChargerVo.getUseStatus());
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }



    private void initLocation() {
        MapLocationUtil.getLocation(getActivity(), false, new MapLocationUtil.LocationCallBack() {
            @Override
            public void callBack(Map<String, Object> map) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(map != null && map.containsKey("code")) {
                            final int code = (int) map.get("code");
                            if(code != 0) {
                                Toast.makeText(getActivity(), "定位失败 code : " + code, Toast.LENGTH_LONG).show();
                            }else {
                                String address = (String) map.get("address");
                                if(!TextUtils.isEmpty(address)) {
                                    binding.tvLocation.setText(address);
                                }
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_willow:
                clearTypeUI();
                binding.tvWillow.setTextColor(getActivity().getResources().getColor(R.color.color_6a95eb));
                requestType = 0;
                break;
            case R.id.tv_lotus:
                clearTypeUI();
                binding.tvLotus.setTextColor(getActivity().getResources().getColor(R.color.color_6a95eb));
                requestType = 1;
                break;
            case R.id.tv_chrysanthemum:
                clearTypeUI();
                binding.tvChrysanthemum.setTextColor(getActivity().getResources().getColor(R.color.color_6a95eb));
                requestType = 2;
                break;
            case R.id.tv_loose:
                clearTypeUI();
                binding.tvLoose.setTextColor(getActivity().getResources().getColor(R.color.color_6a95eb));
                requestType = 3;
                break;
        }
        getChargerList();
    }

    private void clearTypeUI() {
        binding.tvWillow.setTextColor(getActivity().getResources().getColor(R.color.black));
        binding.tvLotus.setTextColor(getActivity().getResources().getColor(R.color.black));
        binding.tvChrysanthemum.setTextColor(getActivity().getResources().getColor(R.color.black));
        binding.tvLoose.setTextColor(getActivity().getResources().getColor(R.color.black));
    }

    /**
     * 请求数据列表数据
     */
    private void getChargerList() {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.CHARGE + "?belong=" + requestType);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                binding.srlRefreshLayout.finishLoadmore(true);
                binding.srlRefreshLayout.finishRefresh(true);
                Toast.makeText(getActivity(), "刷新列表失败:" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                binding.srlRefreshLayout.finishLoadmore(true);
                binding.srlRefreshLayout.finishRefresh(true);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    list.clear();
                    list.addAll((Collection<? extends ChargerVo>) dto.getData());
                    adapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "刷新列表成功： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(), "刷新列表失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private ChargerAdapter.ChargerItemOnClickListener chargerItemOnClickListener = new ChargerAdapter.ChargerItemOnClickListener() {
        @Override
        public void onReserveClick(int position) {
            // 先根据用户id获取用户信息，如果有正在预约中的充电桩和使用中的充电桩，不能预约了
            HomeActivity.currentChargerVo = list.get(position);
            int useStatus = HomeActivity.currentChargerVo.getUseStatus();
            if(useStatus == 1) {
                Toast.makeText(getActivity(), "这个充电桩已经预约了", Toast.LENGTH_SHORT).show();
                return;
            }
            else if(useStatus == 2 || useStatus == 3) {
                Toast.makeText(getActivity(), "这个充电桩正在使用中", Toast.LENGTH_SHORT).show();
                return;
            }
            else if(useStatus == 0) {
//                getUserInfo(position);
                refreshUserInfo("reserve", position);
            }
        }

        @Override
        public void onScanClick(int position) {
            showInputDialog(position);
        }
    };


    private void toScan(int position) {
        HomeActivity.currentChargerVo = list.get(position);
        Intent intent = new Intent(getActivity(), CaptureActivity.class);
        Bundle bundle = new Bundle();
        ZxingConfig config = new ZxingConfig();
        config.setShowAlbum(true);
        config.setShowbottomLayout(true);
        config.setShowFlashLight(true);
        bundle.putSerializable("zxingConfig", config);
        intent.putExtras(bundle);
        CaptureActivity.start(getActivity(), intent);
    }


    // 扫描二维码结果回调(名称 + “-code”)
    private void initScanCallback() {
        CaptureActivity.setScanResultCallback(new ScanResultCallback() {
            @Override
            public void scanFinishResult(String resultStr) {
                startCharger(inputTime);
            }
        });
    }


    private void refreshUserInfo(String methodType, int position) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        String userId = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER + "?userId=" + userId);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    UserVo userVo = dto.getData().get(0);
                    if(userVo.getIsPenalty() == 1) {
                        Toast.makeText(getActivity(), "您预约了充电桩但是没有使用，作为惩罚，30秒内不能预约也不能使用任何充电桩", Toast.LENGTH_LONG).show();
                    }else {
                        if(methodType.equals("reserve")) {
                            getUserInfo(position);
                        }else if(methodType.equals("scan")) {
                            toScan(position);
                        }
                    }
                }else {
                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getUserInfo(int position) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        String userId = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER + "?userId=" + userId);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    UserVo userVo = dto.getData().get(0);
                    // 当前用户已经预约了充电桩
                    if(!TextUtils.isEmpty(userVo.getChargerId())) {
                        Toast.makeText(getActivity(), "您已经预约了充电桩，不能再预约了", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    submitReserveCharger(Config.reserveTimeMinute, list.get(position), position);
                }else {
                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void showInputDialog(int position) {
        if(inputDialog == null) {
            inputDialog = new InputDialog(getActivity());
        }
        inputDialog.setInputDialogOnClickListener(new InputDialog.InputDialogOnClickListener() {
            @Override
            public void submit(String inputTimeStr) {
                inputTime = inputTimeStr;
                HomeActivity.currentChargerVo = list.get(position);
                currentPosition = position;
                refreshUserInfo("scan", position);
            }
        });
        inputDialog.setCancelable(false);
        inputDialog.show();
        inputDialog.clearInput();
    }


    /**
     * 预约充电桩
     * @param mChargerVo
     */
    private void submitReserveCharger(int reserveTimeMinute, ChargerVo mChargerVo, int position) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        mChargerVo.setReserveEndTime(reserveTimeMinute);
        // 设置当前用户id
        mChargerVo.setUserId(SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID));
        String body = GsonUtil.getInstance().toJson(mChargerVo, ChargerVo.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.RESEVER_CHARGE);
        OkhttpClient.asyncRequestPostOrPut(getActivity(), requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "预约失败：" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    // 预约成功后，要保存预约有效时间，显示时候需要(当前时间 + 预约多久充电)
                    String effectiveTime = TimeFormatUtil.getTime(System.currentTimeMillis() + Config.reserveTimeMinute * 60 * 1000);
                    SPUtil.createPrefereceFile(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_RESEVER_TIME, effectiveTime);
                    // 预约成功, 在列表中删除这个item, 当用户点击我的充电桩时，在这里显示
                    ChargerVo refreshChargerVo = (ChargerVo) dto.getData().get(0);
                    list.get(position).setUseStatus(refreshChargerVo.getUseStatus());
                    adapter.notifyDataSetChanged();
                }else {
                    Toast.makeText(getActivity(), "预约失败：" + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
