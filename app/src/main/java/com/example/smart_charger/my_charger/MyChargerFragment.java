package com.example.smart_charger.my_charger;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.charger.ChargerVo;
import com.example.smart_charger.databinding.FragmentMyChargerBinding;
import com.example.smart_charger.dto.BaseDTO;
import com.example.smart_charger.home.HomeActivity;
import com.example.smart_charger.login.UserVo;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.pay.PayActivity;
import com.example.smart_charger.utils.GsonUtil;
import com.example.smart_charger.utils.SPUtil;
import com.example.smart_charger.utils.TimeFormatUtil;
import com.google.gson.reflect.TypeToken;
import com.scan.CaptureActivity;
import com.scan.ScanResultCallback;
import com.scan.bean.ZxingConfig;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 */
public class MyChargerFragment extends Fragment implements View.OnClickListener {

    private FragmentMyChargerBinding binding;

    private String chargerId = "";


    private int endTime;

    private ChargerVo chargerVo;
    // 充电1分钟需要的金额
    private int oneSecondMoney = 10;

    private String inputTime;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_charger, container, false);
        binding.setViewClick(this);
        initScanCallback();
        refresh();
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        switch (){
            // 刷v.getId(新
            case R.id.but_refresh:
                refresh();
                break;
            // 开始充电
            case R.id.but_use:
                binding.rlScanLayout.setVisibility(View.VISIBLE);
                break;
            // 充电完成中的确定按钮
            case R.id.tv_commit:
//                endCharger();
                // 完成充电
                binding.rlReserve.setVisibility(View.GONE);
                binding.tvEmpty.setVisibility(View.VISIBLE);
                binding.rlChargingpileEnd.setVisibility(View.GONE);
                binding.rlUse.setVisibility(View.GONE);
                binding.etInputTime.setText("");
                break;
            // 扫描
            case R.id.iv_scan:
                inputTime = binding.etInputTime.getText().toString();
                if(TextUtils.isEmpty(inputTime)) {
                    Toast.makeText(getActivity(), "请输入需要充电时长", Toast.LENGTH_SHORT).show();
                    return;
                }
                binding.rlScanLayout.setVisibility(View.GONE);
                refreshUserInfo("scan");
                break;
            // 直接支付
            case R.id.but_pay:
                inputTime = binding.etInputTime.getText().toString();
                if(TextUtils.isEmpty(inputTime)) {
                    Toast.makeText(getActivity(), "请输入需要充电时长", Toast.LENGTH_SHORT).show();
                    return;
                }
                int timeI = Integer.parseInt(inputTime);
                Intent intent = new Intent(getActivity(), PayActivity.class);
                intent.putExtra("payMoney", String.valueOf(timeI * oneSecondMoney));
                startActivityForResult(intent, 101);
                break;
            // 结束充电
            case R.id.but_end_charger:
                endCharger();
                binding.etInputTime.setText("");
                break;
            // 取消预约
            case R.id.but_resver_cancel:
                resverCancel();
                binding.etInputTime.setText("");
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 支付成功
        if(requestCode == 101 && resultCode == 99) {
            binding.rlScanLayout.setVisibility(View.GONE);
            refreshUserInfo("pay");
        }
    }

    /**
     * 取消预约
     */
    private void resverCancel() {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
//        Map<String, Object> params = new HashMap<>();
//        chargerVo.setReserveEndTime(0);
        // 设置当前用户id
//        chargerVo.setUserId(SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID));
        String body = GsonUtil.getInstance().toJson(chargerVo, ChargerVo.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.RESEVER_CANCLE_CHARGE);
        OkhttpClient.asyncRequestPostOrPut(getActivity(), requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "取消预约失败：" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    // 取消预约成功, 变成空闲UI
                    binding.rlReserve.setVisibility(View.GONE);
                    binding.tvEmpty.setVisibility(View.VISIBLE);
                    binding.rlChargingpileEnd.setVisibility(View.GONE);
                    binding.rlUse.setVisibility(View.GONE);
                }else {
                    Toast.makeText(getActivity(), "取消预约失败：" + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    /**
     * 开始扫描
     */
    private void scan() {
        Intent intent = new Intent(getActivity(), CaptureActivity.class);
        Bundle bundle = new Bundle();
        ZxingConfig config = new ZxingConfig();
        config.setShowAlbum(true);
        config.setShowbottomLayout(true);
        config.setShowFlashLight(true);
        bundle.putSerializable("zxingConfig", config);
        intent.putExtras(bundle);
        CaptureActivity.start(getActivity(), intent);
        binding.rlScanLayout.setVisibility(View.GONE);
    }


    // 扫描二维码结果回调(名称 + “-code”)
    private void initScanCallback() {
        CaptureActivity.setScanResultCallback(new ScanResultCallback() {
            @Override
            public void scanFinishResult(String resultStr) {
                if(chargerVo == null) {
                    chargerVo = HomeActivity.currentChargerVo;
                }
                if(!TextUtils.isEmpty(resultStr) && resultStr.equals(chargerVo.getName() + "-code")) {
                    startCharger();
                }
            }
        });
    }

    /**
     * 直接支付
     */
    private void toPay() {
        startCharger();
    }


    /**
     * 完成充电
     */
    private void endCharger() {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("id", chargerId);
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.END_CHARGER);
        OkhttpClient.asyncRequestPostOrPut(getActivity(), requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    // 完成充电
                    ChargerVo chargerVo = dto.getData().get(0);
                    binding.rlReserve.setVisibility(View.GONE);
                    binding.tvEmpty.setVisibility(View.VISIBLE);
                    binding.rlChargingpileEnd.setVisibility(View.GONE);
                    binding.rlUse.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 开始充电
     */
    private void startCharger() {
        inputTime = binding.etInputTime.getText().toString();
        if(TextUtils.isEmpty(inputTime)) {
            Toast.makeText(getActivity(), "请输入需要充电时长", Toast.LENGTH_SHORT).show();
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        if(TextUtils.isEmpty(chargerId) && HomeActivity.currentChargerVo != null) {
            chargerId = HomeActivity.currentChargerVo.getId();
        }
        params.put("userId", SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID));
        params.put("id", chargerId);
        params.put("chargerEndTime", 60 * Integer.parseInt(inputTime));
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.START_CHARGER);
        OkhttpClient.asyncRequestPostOrPut(getActivity(), requestParameter, true, "post", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    // 正在充电
                    ChargerVo chargerVo = dto.getData().get(0);
                    binding.rlReserve.setVisibility(View.GONE);
                    binding.tvEmpty.setVisibility(View.GONE);
                    binding.rlChargingpileEnd.setVisibility(View.GONE);
                    binding.rlUse.setVisibility(View.VISIBLE);
                    binding.tvUseLocation.setText("充电桩位置：" + chargerVo.getLocation());
                    endTime = chargerVo.getChargerEndTime();  // 还有多少秒充电结束
//                    long chargerEndTime = System.currentTimeMillis() + chargerVo.getChargerEndTime() * 1000;
                    binding.tvUseTime.setText("充满电量剩余时间：" + endTime + "秒");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                while (endTime > 0) {
                                    Thread.sleep(1000);
                                    endTime --;
                                    handler.sendEmptyMessage(1);
                                }
                                handler.sendEmptyMessage(2);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();
                }
            }
        });
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what == 1) {
                binding.tvUseTime.setText("充满电量剩余时间：" + endTime + "秒");
            }else if(msg.what == 2) {
                binding.rlReserve.setVisibility(View.GONE);
                binding.tvEmpty.setVisibility(View.GONE);
                binding.rlChargingpileEnd.setVisibility(View.VISIBLE);
                binding.rlUse.setVisibility(View.GONE);
            }
        }
    };

    public void refresh() {
        binding.rlScanLayout.setVisibility(View.GONE);
        getUserInfo();
    }


    private void refreshUserInfo(String methodType) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        String userId = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER + "?userId=" + userId);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    UserVo userVo = dto.getData().get(0);
                    if(userVo.getIsPenalty() == 1) {
                        Toast.makeText(getActivity(), "您预约了充电桩但是没有使用，" + userVo.getPenaltyEndTime() + "前不能预约也不能使用任何充电桩", Toast.LENGTH_LONG).show();
                    }else {
                        if(methodType.equals("scan")) {
                            scan();
                        }else if(methodType.equals("pay")) {
                            toPay();
                        }
                    }
                }else {
                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getUserInfo() {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        String userId = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_ID);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER + "?userId=" + userId);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    UserVo userVo = dto.getData().get(0);
                    chargerId = userVo.getChargerId();
                    // 用户没有预约也没有使用任何充电桩
                    if(TextUtils.isEmpty(chargerId)) {
                        binding.rlReserve.setVisibility(View.GONE);
                        binding.tvEmpty.setVisibility(View.VISIBLE);
                        binding.rlChargingpileEnd.setVisibility(View.GONE);
                        binding.rlUse.setVisibility(View.GONE);
                    }else {
                        // 根据充电桩ID获取充电桩信息
                        getChargerInfo(chargerId);
                    }
                }else {
                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getChargerInfo(String chargerId) {
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.CHARGE_ITEM + "?chargerId=" + chargerId);
        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    chargerVo = dto.getData().get(0);
                    int status = chargerVo.getUseStatus();
                    // 空闲状态
                    if(status == 0) {
                        binding.rlReserve.setVisibility(View.GONE);
                        binding.tvEmpty.setVisibility(View.VISIBLE);
                        binding.rlChargingpileEnd.setVisibility(View.GONE);
                        binding.rlUse.setVisibility(View.GONE);
                    }
                    // 预约状态
                    else if(status == 1) {
                        binding.rlReserve.setVisibility(View.VISIBLE);
                        binding.tvEmpty.setVisibility(View.GONE);
                        binding.rlChargingpileEnd.setVisibility(View.GONE);
                        binding.rlUse.setVisibility(View.GONE);
                        binding.tvReserveLocation.setText("充电桩位置：" + chargerVo.getLocation());
                        long chargerEndTime = System.currentTimeMillis() + chargerVo.getReserveEndTime() * 1000;
//                        binding.tvReserveTime.setText("预约有效时间：" + TimeFormatUtil.getTime(chargerEndTime));
                        binding.tvReserveTime.setText("预约有效时间：" + SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, getActivity(), SPUtil.SP_RESEVER_TIME));
                    }
                    // 充电进行中的状态
                    else if(status == 2){
                        binding.rlReserve.setVisibility(View.GONE);
                        binding.tvEmpty.setVisibility(View.GONE);
                        binding.rlChargingpileEnd.setVisibility(View.GONE);
                        binding.rlUse.setVisibility(View.VISIBLE);
                        binding.tvUseLocation.setText("充电桩位置：" + chargerVo.getLocation());
                        long chargerEndTime = System.currentTimeMillis() + chargerVo.getChargerEndTime() * 1000;
                        binding.tvUseTime.setText("充满电量时间：" + TimeFormatUtil.getTime(chargerEndTime));
                    }
                    // 充电完成中的状态
                    else if(status == 3) {
                        binding.rlReserve.setVisibility(View.GONE);
                        binding.tvEmpty.setVisibility(View.GONE);
                        binding.rlChargingpileEnd.setVisibility(View.VISIBLE);
                        binding.rlUse.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


//    /**
//     * 获取当前用户正在使用中的充电桩信息
//     * @param useChargerId
//     */
//    public void refreshChargerUse(String useChargerId) {
//        binding.progressBar.setVisibility(View.VISIBLE);
//        RequestParameter requestParameter = new RequestParameter();
//        Map<String, String> headerMap = new HashMap<>();
//        headerMap.put("Content-Type", "application/json");
//        requestParameter.setHeaderMap(headerMap);
//        requestParameter.setUrl(Config.BASE_URL + NetInterface.CHARGE + "?useChargerId=" + useChargerId);
//        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
//            @Override
//            public void onFailure(String msg) {
//                binding.progressBar.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onSuccess(Map<String, String> map) {
//                binding.progressBar.setVisibility(View.GONE);
//                String responseBody = map.get("responseBody");
//                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
//                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
//                if(dto.getCode() == 200 && dto.getStatus() == 0) {
//                    ChargerVo chargerVo = dto.getData().get(0);
//                    binding.rlReserve.setVisibility(View.GONE);
//                    binding.tvEmpty.setVisibility(View.GONE);
//                    binding.rlChargingpileEnd.setVisibility(View.GONE);
//                    binding.rlUse.setVisibility(View.VISIBLE);
//
//                }else {
//                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }


//    /**
//     * 获取当前用户预约中的充电桩信息
//     * @param reserveChargerId
//     */
//    public void refreshChargerReserve(String reserveChargerId) {
//        binding.progressBar.setVisibility(View.VISIBLE);
//        RequestParameter requestParameter = new RequestParameter();
//        Map<String, String> headerMap = new HashMap<>();
//        headerMap.put("Content-Type", "application/json");
//        requestParameter.setHeaderMap(headerMap);
//        requestParameter.setUrl(Config.BASE_URL + NetInterface.RESEVERING_CHARGE + "?reserveChargerId=" + reserveChargerId);
//        OkhttpClient.asyncRequestGet(getActivity(), requestParameter, true, new RequestCallback() {
//            @Override
//            public void onFailure(String msg) {
//                binding.progressBar.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onSuccess(Map<String, String> map) {
//                binding.progressBar.setVisibility(View.GONE);
//                String responseBody = map.get("responseBody");
//                Type type = new TypeToken<BaseDTO<ChargerVo>>(){}.getType();
//                BaseDTO<ChargerVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
//                if(dto.getCode() == 200 && dto.getStatus() == 0) {
//                    ChargerVo chargerVo = dto.getData().get(0);
//                    binding.rlReserve.setVisibility(View.VISIBLE);
//                    binding.tvEmpty.setVisibility(View.GONE);
//                    binding.rlChargingpileEnd.setVisibility(View.GONE);
//                    binding.rlUse.setVisibility(View.GONE);
//                    binding.tvReserveLocation.setText("充电桩位置：" + chargerVo.getLocation());
//                    binding.tvReserveTime.setText("预约有效期：" + chargerVo.getReserveEndTime() / 60 + "分钟");
//                }else {
//                    Toast.makeText(getActivity(), "获取数据失败： " + dto.getMsg(), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }



}
