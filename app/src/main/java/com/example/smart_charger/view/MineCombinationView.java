package com.example.smart_charger.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.smart_charger.R;

/**
 * 我的页面组合控件
 */
public class MineCombinationView extends RelativeLayout {

    private int leftImgResource;
    private int rightImgResource;
    private String title;

    private ImageView leftImg;
    private ImageView rightImg;
    private TextView titleText;

    public MineCombinationView(Context context) {
        this(context, null);
    }

    public MineCombinationView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MineCombinationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MineCombinationView);
        title = typedArray.getString(R.styleable.MineCombinationView_title_text);
        leftImgResource = typedArray.getResourceId(R.styleable.MineCombinationView_left_src, R.drawable.ic_login_psw);
        rightImgResource = typedArray.getResourceId(R.styleable.MineCombinationView_right_src, R.drawable.ic_login_psw);
        typedArray.recycle();

        LayoutInflater.from(context).inflate(R.layout.view_combination_mine, this, true);

        leftImg = findViewById(R.id.left_img);
        rightImg = findViewById(R.id.right_img);
        titleText = findViewById(R.id.title_text);

        Glide.with(context).load(leftImgResource).into(leftImg);
        Glide.with(context).load(rightImgResource).into(rightImg);
        titleText.setText(title);
    }
}
