package com.example.smart_charger.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.smart_charger.R;

/**
 */
public class InputDialog extends AlertDialog {

    private EditText etInputView;
    private Button butCancle;
    private Button butSubmit;

    private Context context;

    private InputDialogOnClickListener mInputDialogOnClickListener;

    public InputDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_input_layout);
        etInputView = findViewById(R.id.et_input_view);
        butCancle = findViewById(R.id.but_cancle);
        butCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputDialog.this.cancel();
            }
        });
        butSubmit = findViewById(R.id.but_submit);
        butSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != mInputDialogOnClickListener) {
                    if(TextUtils.isEmpty(etInputView.getText().toString())) {
                        Toast.makeText(context, "请输入预约时间", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    InputDialog.this.cancel();
                    mInputDialogOnClickListener.submit(etInputView.getText().toString());
                }
            }
        });
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
    }

    public interface InputDialogOnClickListener {
        // reserveTimeMinute分钟后，开始使用
        void submit(String inputTime);
    }

    public void setInputDialogOnClickListener(InputDialogOnClickListener mInputDialogOnClickListener) {
        this.mInputDialogOnClickListener = mInputDialogOnClickListener;
    }

    public void clearInput() {
        if(null != etInputView) {
            etInputView.setText("");
        }
    }
}
