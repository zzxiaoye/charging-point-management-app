package com.example.smart_charger.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart_charger.R;

public class PayDialog extends AlertDialog {

    private Context context;

    private Button but_pay;
    private ImageView iv_close;
    private TextView tv_pay_money;

    private String payMoney;

    private PayOnClickListener mPayOnClickListener;

    public PayDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_pay_layout);
        but_pay = findViewById(R.id.but_pay);
        iv_close = findViewById(R.id.iv_close);
        tv_pay_money = findViewById(R.id.tv_pay_money);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_pay_money.setText("￥");
                PayDialog.this.cancel();
            }
        });

        but_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null != mPayOnClickListener && !TextUtils.isEmpty(payMoney)) {
                    mPayOnClickListener.payOnClick(payMoney);
                    tv_pay_money.setText("￥");
                    PayDialog.this.cancel();
                }
            }
        });
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
    }


    public void setPayMoney(String payMoney) {
        this.payMoney = payMoney;
        tv_pay_money.setText("￥" + payMoney);
    }

    public interface PayOnClickListener {
        void payOnClick(String payMoney);
    }

    public void setPayOnClickListener(PayOnClickListener mPayOnClickListener) {
        this.mPayOnClickListener = mPayOnClickListener;
    }
}
