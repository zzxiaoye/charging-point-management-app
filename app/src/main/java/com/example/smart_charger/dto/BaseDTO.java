package com.example.smart_charger.dto;

import java.io.Serializable;
import java.util.List;

public class BaseDTO<T> implements Serializable {

    private int code;  // 200 表示接口调用成功
    private String msg;
    private int status;  // 0 表示返回正确的数据   1 错误
    private List<T> data;  // 具体的实例

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
