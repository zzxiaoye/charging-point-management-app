package com.example.smart_charger.base;

import android.os.Bundle;
import android.view.Window;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.smart_charger.R;
import com.example.smart_charger.utils.StatusBarUtils;

/**
 * activity基类
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setStatusBarColor(getColor(R.color.white), true);
    }


    /**
     * 设置状态栏颜色
     *
     * @param color 状态栏颜色
     * @param dark  状态栏文字颜色(true 黑色  false 白色)
     */
    public void setStatusBarColor(@ColorInt int color, boolean dark) {
        StatusBarUtils.setStatusBarTextColor(this, dark);
        getWindow().setStatusBarColor(color);
    }
}
