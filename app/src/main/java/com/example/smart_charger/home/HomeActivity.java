package com.example.smart_charger.home;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.smart_charger.R;
import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.charger.ChargerFragment;
import com.example.smart_charger.charger.ChargerVo;
import com.example.smart_charger.databinding.ActivityHomeBinding;
import com.example.smart_charger.manager.ManagerFragment;
import com.example.smart_charger.mine.MineFragment;
import com.example.smart_charger.my_charger.MyChargerFragment;
import com.example.smart_charger.utils.SPUtil;
import com.example.smart_charger.utils.SettingUtil;
import com.lcw.library.imagepicker.ImagePicker;
import com.scan.CaptureActivity;
import com.scan.ScanResultCallback;

/**
 */
public class HomeActivity extends BaseActivity implements View.OnClickListener {

    private ActivityHomeBinding binding;

    private ManagerFragment mManagerFragment;
    private ChargerFragment mChargerFragment;
    private MyChargerFragment mMyChargerFragment;
    private MineFragment mMineFragment;

    private FragmentManager mFragmentManager;
    private FragmentTransaction transaction;

    // 选项卡选中的颜色
    private int checkColor;
    // 选项卡未选中的颜色
    private int noCheckColor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        binding.setViewClick(this);
        init();
        String userNumber = SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, this, SPUtil.SP_ID);
        // 管理员
        if(!TextUtils.isEmpty(userNumber) && userNumber.equals("admin")) {
            binding.rlManagerLayout.setVisibility(View.VISIBLE);
            binding.rlChargerLayout.setVisibility(View.GONE);
            binding.rlMyChargerLayout.setVisibility(View.GONE);
            setTabSelection(0);
        }else {
            binding.rlChargerLayout.setVisibility(View.VISIBLE);
            binding.rlMyChargerLayout.setVisibility(View.VISIBLE);
            binding.rlManagerLayout.setVisibility(View.GONE);
            setTabSelection(1);
        }
    }

    private void init() {
        mFragmentManager = getSupportFragmentManager();
        checkColor = getResources().getColor(R.color.white);
        noCheckColor = getResources().getColor(R.color.common_blue_2fa2f6);
        initScanCallback();
    }

    private void setTabSelection(int index) {
        transaction = mFragmentManager.beginTransaction();
        clearSelection();
        hideFragments();
        switch (index) {
            case 0:
                binding.tvTitle.setText("查询管理");
                SettingUtil.setColor(binding.tvManager, getResources().getColor(R.color.main_background_color));
                if (mManagerFragment == null) {
                    mManagerFragment = new ManagerFragment();
                    transaction.add(R.id.fl_center_layout, mManagerFragment);
                }else {
                    transaction.show(mManagerFragment);
                }
                break;
            case 1:
                binding.tvTitle.setText("充电桩");
                SettingUtil.setColor(binding.tvCharger, getResources().getColor(R.color.main_background_color));
                if (mChargerFragment == null) {
                    mChargerFragment = new ChargerFragment();
                    transaction.add(R.id.fl_center_layout, mChargerFragment);
                }else {
                    transaction.show(mChargerFragment);
                }
                break;
            case 2:
                binding.tvTitle.setText("我的充电桩");
                SettingUtil.setColor(binding.tvMyCharger, getResources().getColor(R.color.main_background_color));
                if (mMyChargerFragment == null) {
                    mMyChargerFragment = new MyChargerFragment();
                    transaction.add(R.id.fl_center_layout, mMyChargerFragment);
                }else {
                    transaction.show(mMyChargerFragment);
                }
                break;
            case 3:
                binding.tvTitle.setText("个人中心");
                SettingUtil.setColor(binding.tvMine, getResources().getColor(R.color.main_background_color));
                if (mMineFragment == null) {
                    mMineFragment = new MineFragment();
                    transaction.add(R.id.fl_center_layout, mMineFragment);
                }else {
                    transaction.show(mMineFragment);
                }
                break;
        }
        transaction.commit();
    }

    private void clearSelection() {
        SettingUtil.setColor(binding.tvManager, getResources().getColor(R.color.black));
        SettingUtil.setColor(binding.tvCharger, getResources().getColor(R.color.black));
        SettingUtil.setColor(binding.tvMyCharger, getResources().getColor(R.color.black));
        SettingUtil.setColor(binding.tvMine, getResources().getColor(R.color.black));
    }

    private void hideFragments() {
        if (mManagerFragment != null) {
            transaction.hide(mManagerFragment);
        }
        if (mChargerFragment != null) {
            transaction.hide(mChargerFragment);
        }
        if (mMyChargerFragment != null) {
            transaction.hide(mMyChargerFragment);
        }
        if (mMineFragment != null) {
            transaction.hide(mMineFragment);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if(mMineFragment == null && fragment instanceof MineFragment) {
            mMineFragment = (MineFragment) fragment;
        }
        else if(mManagerFragment == null && fragment instanceof ManagerFragment) {
            mManagerFragment = (ManagerFragment) fragment;
        }
        else if(mChargerFragment == null && fragment instanceof ChargerFragment) {
            mChargerFragment = (ChargerFragment) fragment;
        }
        else if(mMyChargerFragment == null && fragment instanceof MyChargerFragment) {
            mMyChargerFragment = (MyChargerFragment) fragment;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(transaction != null) {
            if(mMineFragment != null) {
                transaction.remove(mMineFragment);
                mMineFragment = null;
            }
            if(mManagerFragment != null) {
                transaction.remove(mManagerFragment);
                mManagerFragment = null;
            }
            if(mChargerFragment != null) {
                transaction.remove(mChargerFragment);
                mChargerFragment = null;
            }
            if(mMyChargerFragment != null) {
                transaction.remove(mMyChargerFragment);
                mMyChargerFragment = null;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_manager_layout:
                setTabSelection(0);
                break;
            case R.id.rl_charger_layout:
                setTabSelection(1);
                break;
            case R.id.rl_my_charger_layout:
                refreshMyCharge();
                setTabSelection(2);
                break;
            case R.id.rl_mine_layout:
                setTabSelection(3);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 选择头像返回结果
        if (requestCode == MineFragment.REQUEST_SELECT_IMAGES_CODE && resultCode == RESULT_OK) {
            String imgPath = data.getStringArrayListExtra(ImagePicker.EXTRA_SELECT_IMAGES).get(0);
            if(mMineFragment != null) {
                mMineFragment.showHeaderImg(imgPath);
            }
        }
    }


    /**
     * 刷新我的充电桩
     */
    public void refreshMyCharge() {
        if(mMyChargerFragment != null) {
            mMyChargerFragment.refresh();
        }
    }


    public static ChargerVo currentChargerVo;


    // 扫描二维码结果回调(名称 + “-code”)
    private void initScanCallback() {
        CaptureActivity.setScanResultCallback(new ScanResultCallback() {
            @Override
            public void scanFinishResult(String resultStr) {
                Log.e("TAG", "resultStr == " + resultStr);
//                if(HomeActivity.currentChargerVo != null && !TextUtils.isEmpty(resultStr) && resultStr.equals(HomeActivity.currentChargerVo.getName() + "-code") && mChargerFragment != null) {
//                    mChargerFragment.startCharger("");
//                }
            }
        });
    }

}
