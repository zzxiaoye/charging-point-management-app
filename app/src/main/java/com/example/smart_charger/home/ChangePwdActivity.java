package com.example.smart_charger.home;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.example.smart_charger.Config;
import com.example.smart_charger.R;
import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.databinding.ActivityChangePwdBinding;
import com.example.smart_charger.dto.BaseDTO;
import com.example.smart_charger.login.LoginActivity;
import com.example.smart_charger.login.UserVo;
import com.example.smart_charger.net.NetInterface;
import com.example.smart_charger.net.OkhttpClient;
import com.example.smart_charger.net.RequestCallback;
import com.example.smart_charger.net.RequestParameter;
import com.example.smart_charger.utils.GsonUtil;
import com.example.smart_charger.utils.SPUtil;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 */
public class ChangePwdActivity extends BaseActivity implements View.OnClickListener {

    private ActivityChangePwdBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_pwd);
        binding.setViewClick(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                submit();
                break;
        }
    }


    private void submit() {
        String oldPwd = binding.etOldPwd.getText().toString();
        String newPwd = binding.etNewPwd.getText().toString();
        String confirmNewPwd = binding.etConfirmNewPwd.getText().toString();
        if(TextUtils.isEmpty(oldPwd) || TextUtils.isEmpty(newPwd) || TextUtils.isEmpty(confirmNewPwd)) {
            Toast.makeText(this, "请输入完整信息", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!newPwd.equals(confirmNewPwd)) {
            Toast.makeText(this, "两次新密码输入不一致", Toast.LENGTH_SHORT).show();
            return;
        }

        binding.btnSubmit.setEnabled(false);
        binding.progressBar.setVisibility(View.VISIBLE);
        RequestParameter requestParameter = new RequestParameter();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");
        requestParameter.setHeaderMap(headerMap);
        // 参数
        Map<String, Object> params = new HashMap<>();
        params.put("id", SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, this, SPUtil.SP_ID));
        params.put("newPassword", newPwd);
        params.put("password", oldPwd);
        String body = GsonUtil.getInstance().toJson(params, Map.class);
        requestParameter.setBody(body);
        requestParameter.setUrl(Config.BASE_URL + NetInterface.USER_CHANGE_PAW);
        OkhttpClient.asyncRequestPostOrPut(this, requestParameter, true, "put", new RequestCallback() {
            @Override
            public void onFailure(String msg) {
                binding.btnSubmit.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(ChangePwdActivity.this, "更改密码失败：" + msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(Map<String, String> map) {
                binding.btnSubmit.setEnabled(true);
                binding.progressBar.setVisibility(View.GONE);
                String responseBody = map.get("responseBody");
                Type type = new TypeToken<BaseDTO<UserVo>>(){}.getType();
                BaseDTO<UserVo> dto = GsonUtil.getInstance().fromJson(responseBody, type);
                if(dto.getCode() == 200 && dto.getStatus() == 0) {
                    Toast.makeText(ChangePwdActivity.this, "更改密码成功", Toast.LENGTH_SHORT).show();
                    ChangePwdActivity.this.finish();
                }else {
                    Toast.makeText(ChangePwdActivity.this, "更改密码失败：" + dto.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
