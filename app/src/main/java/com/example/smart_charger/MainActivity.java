package com.example.smart_charger;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.example.smart_charger.base.BaseActivity;
import com.example.smart_charger.databinding.ActivityMainBinding;
import com.example.smart_charger.home.HomeActivity;
import com.example.smart_charger.login.LoginActivity;
import com.example.smart_charger.utils.SPUtil;
import com.example.smart_charger.utils.TimerDownUtil;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ActivityMainBinding binding;

    private final int REQUEST_PERMISSION_CODE = 0;

    private TimerDownUtil mTimerDownUtil;
    private ArrayList<String> requestPermissionList = new ArrayList<String>();
    private String[] listPermission = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setViewClick(this);
        checkPermissions(listPermission);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mTimerDownUtil != null) {
            mTimerDownUtil.cancel();
        }
    }

    private void initTime() {
        if(mTimerDownUtil != null) {
            mTimerDownUtil.cancel();
        }
        mTimerDownUtil = new TimerDownUtil(3, new TimerDownUtil.Listener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onTick(int seconds) {
                if(binding.tvTime != null) {
                    binding.tvTime.setText(seconds + "秒");
                }
            }

            @Override
            public void onComplete() {
                next();
            }

            @Override
            public void onCanceled() {

            }
        });
        mTimerDownUtil.start();
    }

    @Override
    public void onClick(View v) {
        next();
    }


    private void next() {
        if(TextUtils.isEmpty(SPUtil.getPrefereceFileKeyValue(SPUtil.SP_FILE_NAME, this, SPUtil.SP_ID))) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }else {
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
        }
        MainActivity.this.finish();
    }


    private void checkPermissions(String[] listPermission) {
        boolean checkPermission = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String permission : listPermission) {
                if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissionList.add(permission);
                }
            }
            if (requestPermissionList.size() == 0) {
                checkPermission = true;
            } else {
                checkPermission = false;
            }
        } else {
            checkPermission = true;
        }
        if (checkPermission) {
            Toast.makeText(this, "您开通了所有权限，请继续", Toast.LENGTH_LONG).show();
            initTime();
        } else {
            requestPermission(requestPermissionList, REQUEST_PERMISSION_CODE);
        }
    }

    public void requestPermission(List<String> requestPermissionList, int requestCode) {
        ActivityCompat.requestPermissions(this, requestPermissionList.toArray(new String[requestPermissionList.size()]), requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE) {
            for (int grant : grantResults) {
                if (grant != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "您拒绝了权限,将退出应用", Toast.LENGTH_LONG).show();
                    MainActivity.this.finish();
                    return;
                }
            }
            Toast.makeText(this, "您开通了所有权限，请继续", Toast.LENGTH_LONG).show();
            initTime();
        }
    }
}